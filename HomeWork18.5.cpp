#include <iostream>

class STACK
{
private:
    int* stack;
    int count;

public:

    STACK()
    {
        stack = nullptr;
        count = 0;

    }
    ~STACK()
    {

        if (count > 0) delete[] stack;

    }

    void push(int item)
    {
        int* tmp;
        tmp = stack;
        stack = new int[count + 1];
        count++;
        for (int i = 0; i < count - 1; i++)
        {
            stack[i] = tmp[i];
        }
        stack[count - 1] = item;
        if (count > 1) delete[] tmp;
    }
    
    int pop()
    {
        if (count == 0) return 0;
        count--;
        return stack[count];
    }

    int Size()
    {
        return count;
    }

    void Print()
    {

        int* p;
        p = stack;
        std::cout << "Stack: " << std::endl;
        std::cout << "\n";
        if (count == 0) std::cout << "is Empty!" << std::endl;
        for (int i = 0; i < count; i++)
        {
            std::cout << "Item [" << i << "] = " << *p << "\n" << std::endl;
            p++;
        }

    }

};


int main()
{
    

    STACK st;
    st.Print();
    
    int size;
    int j=0;
    int elem;
    std::cout << "\n";
    std::cout << "Vvedite dlinu Steka: ";
    std::cin >> size;
    std::cout << "\n";
    while (j++ < size)
    {
        std::cout << "Vvedite element nomer " << j<<" : ";
        std::cin >> elem;
        st.push(elem);
    }
    std::cout << "\n";
    st.Print();
    std::cout << "\n";

    int a = st.pop();
    std::cout << "Posledniy element = " << a <<"\n"<< std::endl;
	std::cout << "Udalim posledniy element" <<"\n"<< std::endl;
    st.Print();
	std::cout << "\n";

Question:

    int temp;
    std::cout << "VVedite 4islo 1 dlya doblenia novogo elementa v stek, 4islo 2 dlya udalenia poslednego 4isla v steke ili 4islo 3 dlya vihoda! " << "\n" << std::endl;
    std::cin >> temp;
    std::cout << "\n";

    switch (temp)
    {
    case 1:
        std::cout << "vvedite noviy elem : ";
        std::cin >> elem;
        st.push(elem);
        st.Print();
        goto Question;
        break;
    case 2:
        a = st.pop();
        std::cout << "Posledniy element = " << a << "\n" << std::endl;
        std::cout << "Udaleno!" << "\n" << std::endl;
		st.Print();
        goto Question;
		break;
    case 3:
        std::cout <<"\n"<< "Bye Bye!";
        return 0;
        break;
    default:
        std::cout << "Vi vveli ne korrektnoe 4islo, povtorite pls)" << "\n";
        goto Question;
        break;
    }
}

